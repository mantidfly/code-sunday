# O warsztatach

## Informacje wstępne

- warsztaty są dla początkujących
- mówimy do siebie po imieniu
- zadajemy pytania w dowolnym momencie
- pytamy do skutku - jak nie rozumiemy, to wina prowadzącego! :D
- jeśli się gubimy, natychmiast prosimy o pomoc - później będzie trudniej nadrobić zaległości
- jeśli prowadzący robi coś źle - NATYCHMIAST ZWRACAMY MU UWAGĘ!!!
  - zbyt szybkie tłumaczenie lub zbyt wolne tłumaczenie
  - zbyt szybkie przełączanie między ekranami
  - niewyraźne mówienie
  - zbyt częste gadanie o pierdołach - w końcu chcemy się uczyć! :D
- korzystamy z Visual Studio Code + Firefox
- w kodzie stosujemy anglojęzyczne nazwy (jeśli nie jest to istotny problem!)

## Kim jestem

- aplikacja do zarządzania kursami na UAM (Java, JEE, EJB 3.1, JavaScript)
- aplikacja symulująca cyberataki (JavaScript, PHP, CakePHP, Bash)
- strony internetowe (PHP, JavaScript, MySQL)

  - https://reaktor.pwn.pl/
  - http://unpacking.design/
  - http://www.martakostecka.pl/
- Latice - https://itunes.apple.com/app/latice/id1109633103

  - przenoszenie AI do chmury (C#, .NET Core, Unity)
  - automatyzacja testów na urządzeniach z iOS (Python)
  - wersja Facebookowa gry (C#, Unity, JavaScript)
- SWPS Trains - eksperyment psychologiczny (JavaScript, PHP, Docker)
- Skillfortress - http://skillfortress.com/ (JavaScript, React, C#, .NET Core, Bash, Docker)

## Rada

- Nie idźcie w moje ślady!
- Specjaliści:
  - awansują szybciej
  - mogą się ograniczyć się do mniejszej liczby technologii
  - zarabiają lepiej!
- Chyba że, tak jak ja, jesteście strasznie ciekawscy - tutaj nic nie poradzę! :D

## Programowanie w JavaScript

- HTML + CSS + JS

## Plan warsztatów

- Co zbudujemy?
- 5 etapów rozwoju
- Plan minimum
  - HTML
  - CSS
  - JS - podstawy składni języka
    - zmienne
    - operatory
    - instrukcja warunkowa
    - pętla for
    - pętla while
    - pętla do while
- Plan potencjalnie wykonalny
  - JS - organizacja kodu
    - funkcje
    - biblioteki
    - frameworki
- **Turbo**plan :D (opcjonalny?)
  - JS - programowanie obiektowe
    - klasy
    - obiekty
    - pola
    - metody
    - `this`
- Q&A - zawód programisty

# Narzędzia programistyczne (na dziś)

- edytor (IDE) - Visual Studio Code
- Firefox + inspektor kodu
  - konsola (`console.log()`)
  - inspektor
  - edytor stylów


# HTML (HyperText Markup Language)

- co to jest i do czego służy

- elementy HTMLa

  - znaczniki
  - atrybuty
    - `id`
    - `class`
    - `name`
    - `style`
  - własne atrybuty (custom attributes)
    - `<div data-myCustomAttribute="jakaś wartość"></div>`
  - komentarze

- szkielet strony www

  - `<!DOCTYPE html>`
  - `<html>`
  - `<head>`
    - `<meta charset="UTF-8">`
    - `<title>Nazwa strony</title>`
  - `<body>`
  - przykład http://skillfortress.com

- często używane znaczniki

  - `<h1>Ważny nagłówek</h1>`

  - `<p>Paragraf tekstu</p>`

  - `<span>Fragment, który chcemy wyróżnić</span>`

  - `<a href="http://google.com">google</a>`

  - `<img src="http://google.com" />`

  - ```html
    <div>
    	<h1>Treść nagłówka</h1>
    	<p>Bardzo ważna wiadomość</p>
    </div>
    ```

- HTML5

  - `<video>`

    ```html
    <video width="320" height="240" controls>
    	<source src="movie.mp4" type="video/mp4">
      	<source src="movie.ogg" type="video/ogg">
    	Your browser does not support the video tag.
    </video> 
    ```

  - `<audio>`

    ```html
    <audio controls>
    	<source src="horse.ogg" type="audio/ogg">
    	<source src="horse.mp3" type="audio/mpeg">
    	Your browser does not support the audio element.
    </audio> 
    ```

  - semantyka
    - `<header>`
    - `<footer>`
    - `<article>`
    - `<aside>`
    - `<nav>`
    - `<strong>` vs `<b>`

- ### Zadanie:

  Przygotuj prostą stronę zawierającą następujące elementy:

  - `Imię` i `nazwisko`, które po kliknięciu przenoszą do Twojego profilu na Facebooku/LinkedIn
  - paragraf tekstu zawierający Twój cel programistyczny na najbliższy rok (1 zdanie)

- ### Projekt #1:

  Szkielet gry:

  - Przygotuj strukturę niezbędną do utworzenia gry
    - level -  `<div>`
      - title: "My first demo game!" - `<h6>`
      - star - `<span>`
      - ship - `<img>`
      - enemy - `<img>`

# CSS (Cascading Style Sheets)

- Czym jest CSS?
  - selektory (selectors)
  - własności (properties)
  - jednostki (units)
    - px
    - %
    - rem
    - em
  - komentarze

- Box model

  - content
  - padding
  - border
  - margin

- Jak dołączyć CSS do strony html?

  - znacznik `style`

  ```css
  <style type="text/css">
  	body {
          background-color: red;
  	}
  </style>
  ```

  - CSS w osobnym pliku

    `<link rel="stylesheet" type="text/css" href="styles.css" />`

    

- Często używane własności
  - font-size
  - color
  - background-color
  - width
  - height
  - display (none/inline/block)
  - position (static/relative/absolute/fixed)
  - top/bottom, left/right

- ### Projekt #2:

  Wygląd gry:

  - Ostyluj stronę gry zgodnie z wymaganiami:
  - `level`
    - dodaj klasę `level`
    - wysokość:  `600px`
    - szerokość: `600px`
    - kolor tła: `#121111`
    - umieść na środku ekranu (w pionie i poziomie)
  - `h6`
    - dodaj klasę `title`
    - umieść na pozycji `x=20` `y=20`
    - ustaw wielkość fontu na `15px`
    - wybierz rodzaj fontu: `Arial`
    - usuń margines i padding
  - `ship`
    - dodaj klasę `ship`
    - wysokość: `40px`
    - szerokość: `40px`
    - umieść na pozycji `x=300` `y=500`
  - `star`
    - dodaj klasę `star`
    - wysokość: `3px`
    - szerokość: `3px`
    - umieść na pozycji `x=20` `y=20`
    - kolor tła: `silver`
  - `enemy`
    - dodaj klasę `enemy`
    - wysokość: `40px`
    - szerokość: `40px`
    - umieść na pozycji `x=300` `y=60`

# JS (JavaScript)

## Wprowadzenie

- czym jest JS?
- front-end? back-end?
- ES6, ES7, ...
- babel/transpilacja
- dołączanie skryptów JS do strony HTML

## Podstawy składni języka

- zmienne

  - `var`, `let`, `const`
  - 7 podstawowych typów danych
    - number
    - string
    - boolean
    - symbol
    - null
    - undefined
    - object
  - wbudowane typy złożone
    - Array
  - **Jak działa hoisting?**

- operatory
  - `+` `-` `*` `**` `%` `=`
  - `++` `--`
  - `+=` `-=` `*=`

- instrukcja warunkowa

  ```javascript
  if (warunek) {
      ...
  } else if (inny warunek) {
      ...
  } else {
      ...
  }
  ```

- pętla `for`

  ```javascript
  for (let i; i < 10; i++) {
      ...
  }
  ```

- pętla `while`

  ```javascript
  while (warunek) {
      ...
  }
  ```

- pętla `do/while`

  ```javascript
  do {
      ...
  } while (warunek);
  ```

### Projekt #3:

1. Zaimplementuj sterowanie statkiem
   - statek ma mieć możliwość poruszania we wszystkich kierunkach
2. Animuj gwiazdę
   - gwiazda mają lecieć pionowo, od góry do dołu z prędkością `10px`/`16ms`
   - po wyjściu za ekran gwiazda ma wrócić na górę ekranu i kontynuować lot
3. Utwórz jeszcze 6 gwiazd i rozmieść je po planszy
4. Zmień kod tak, żeby móc modyfikować prędkość wszystkich gwiazd za pomocą jednej zmiennej
5. Ustaw prędkość gwiazd na `2px`/`16ms`
6. Animuj przeciwnika
   - przeciwnik ma lecieć pionowo, od góry do dołu
   - po wyjściu za ekran przeciwnik ma wrócić na górę ekranu, ale ma startować z LOSOWEGO miejsca

## Funkcje

- Czym jest funkcja?

- argumenty funkcji

- `return`

- arrow function

### Projekt #4:

1. Zrefaktoryzuj projekt używając mechanizmu funkcji, tak żeby nie powielać niepotrzebnie fragmentów kodu.


## Programowanie obiektowe

- klasa
- obiekt
- metoda
- pole
- `this`
- dziedziczenie

### Projekt #5:

1. Zrefaktoryzuj kod zamieniając funkcje na klasy i metody
2. Dodaj nowy rodzaj przeciwnika, który oprócz poruszania się z góry na dół będzie również poruszał się na boki. (Niech leci zygzakiem lub po prostu niech go znosi na bok.)