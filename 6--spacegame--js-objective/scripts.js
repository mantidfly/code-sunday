// creating level
let levelWidth = 600;
let levelHeight = 600;
let level = document.getElementById('level');
level.style.width = levelWidth + 'px';
level.style.height = levelHeight + 'px';

class Ship {
    constructor(x, y) {
        this.x = x;
        this.y = y;
        let ship = document.createElement('img');
        ship.classList.add('ship');
        ship.setAttribute('src', 'ship.png');
        ship.style.left = x + 'px';
        ship.style.top = y + 'px';
        level.appendChild(ship);
        this.element = ship;
    }

    move(e) {
        // updating ship position
        if (e.keyCode === 37) {
            this.x -= 20;
        } else if (e.keyCode === 39) {
            this.x += 20;
        } else if (e.keyCode === 38) {
            this.y -= 20;
        } else if (e.keyCode === 40) {
            this.y += 20;
        }
    
        // checking if ship didn't go out of level
        this.x = this.x <= 0 ? 0 : this.x;
        this.x = this.x >= levelWidth - 40 ? levelWidth - 40 : this.x;
        this.y = this.y <= 0 ? 0 : this.y;
        this.y = this.y >= levelHeight - 40 ? levelHeight - 40 : this.y;
    
        // updating ship picture
        this.element.style.left = this.x + 'px';
        this.element.style.top = this.y + 'px';
    }
}

class Star {
    constructor(x, y) {
        this.x = x;
        this.y = y;
        let star = document.createElement('span');
        star.classList.add('star');
        star.style.left = x + 'px';
        star.style.top = y + 'px';
        level.appendChild(star);
        this.element = star;
    }

    animate() {
        this.y += 2;
        this.y = this.y >= levelHeight ? 0 : this.y;
        this.element.style.top = this.y + 'px';
    }
}

class Enemy {
    constructor(x, y) {
        this.x = x;
        this.y = y;
        let enemy = document.createElement('img');
        enemy.classList.add('enemy');
        enemy.setAttribute('src', 'enemy-1.png');
        enemy.style.left = x + 'px';
        enemy.style.top = y + 'px';
        level.appendChild(enemy);
        this.element = enemy;
    }

    animate(speed) {
        this.y += speed;
        this.y = this.y >= levelHeight - 40 ? 0 : this.y;
        this.element.style.top = this.y + 'px';
    
    }
}

// creating game objects
let ship = new Ship(300, 500);

let stars = [
    new Star(100, 300),
    new Star(300, 400),
    new Star(500, 500),
    new Star(150, 500),
];

let enemies = [
    new Enemy(150, 60),
    new Enemy(250, 20),
    new Enemy(450, 20),
];

// animating objects
setInterval(() => {
    // stars
    for (let i = 0; i < stars.length; i++) {
        stars[i].animate();
    }
    
    // enemy
    for (let i = 0; i < enemies.length; i++) {
        enemies[i].animate(i + 1);
    }
}, 16);

// handling keyboard
document.addEventListener('keydown', (e) => {
    ship.move(e);
});