// creating level
let levelWidth = 600;
let levelHeight = 600;
let level = document.getElementById('level');
level.style.width = levelWidth + 'px';
level.style.height = levelHeight + 'px';

function createShip(x, y) {
    let ship = document.createElement('img');
    ship.classList.add('ship');
    ship.setAttribute('src', 'ship.png');
    ship.style.left = x + 'px';
    ship.style.top = y + 'px';
    level.appendChild(ship);
    return {
        x: x, 
        y: y,
        element: ship,
    };
}

function moveShip(ship, e) {
    // updating ship position
    if (e.keyCode === 37) {
        ship.x -= 20;
    } else if (e.keyCode === 39) {
        ship.x += 20;
    } else if (e.keyCode === 38) {
        ship.y -= 20;
    } else if (e.keyCode === 40) {
        ship.y += 20;
    }

    // checking if ship didn't go out of level
    ship.x = ship.x <= 0 ? 0 : ship.x;
    ship.x = ship.x >= levelWidth - 40 ? levelWidth - 40 : ship.x;
    ship.y = ship.y <= 0 ? 0 : ship.y;
    ship.y = ship.y >= levelHeight - 40 ? levelHeight - 40 : ship.y;

    // updating ship picture
    ship.element.style.left = ship.x + 'px';
    ship.element.style.top = ship.y + 'px';
}

function createStar(x, y) {
    let star = document.createElement('span');
    star.classList.add('star');
    star.style.left = x + 'px';
    star.style.top = y + 'px';
    level.appendChild(star);
    return {
        x: x,
        y: y,
        element: star,
    };
}

function animateStar(star) {
    star.y += 2;
    star.y = star.y >= levelHeight ? 0 : star.y;
    star.element.style.top = star.y + 'px';
}

function createEnemy(x, y) {
    let enemy = document.createElement('img');
    enemy.classList.add('enemy');
    enemy.setAttribute('src', 'enemy-1.png');
    enemy.style.left = x + 'px';
    enemy.style.top = y + 'px';
    level.appendChild(enemy);
    return {
        x: x,
        y: y,
        element: enemy,
    };
}

function animateEnemy(enemy, speed) {
    enemy.y += speed;
    enemy.y = enemy.y >= levelHeight - 40 ? 0 : enemy.y;
    enemy.element.style.top = enemy.y + 'px';
}

// creating game objects
let ship = createShip(300, 500);

let star1 = createStar(100, 300);
let star2 = createStar(300, 400);
let star3 = createStar(500, 500);
let star4 = createStar(150, 500);

let enemy1 = createEnemy(150, 60);
let enemy2 = createEnemy(250, 20);
let enemy3 = createEnemy(450, 20);

// animating objects
setInterval(() => {
    // stars
    animateStar(star1);
    animateStar(star2);
    animateStar(star3);
    animateStar(star4);

    // enemy
    animateEnemy(enemy1, 2);
    animateEnemy(enemy2, 1);
    animateEnemy(enemy3, 3);
}, 16);

// handling keyboard
document.addEventListener('keydown', (e) => {
    moveShip(ship, e);
});